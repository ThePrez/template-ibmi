## Template Project for IBM i
This project is a generic template for any standalone IBM i project. It requires
IBM i 7.3 or newer.

Out of the box it comes with a very simplistic demo application covering a 
service program and a web service.


### Folder Structure

* docs - project documentation (like project ILEDocs, draw.io diagram or Swagger files)
* examples - contains example files (like program examples)
* include - contains all copybooks needed to build the project which are not covered 
            by the public copybook folder of the server environment (f. e. /usr/local/include)
* src - contains any source of this project (except copybooks)
* src/database - contains all SQL scripts needed to setup the database
* src/web - contains all sources necessary to create the web services
* src/objects - contains all sources needed to create any objects which are not 
                covered by any other folder


### Prerequisites
This template project uses [ILEastic](https://github.com/sitemule/ILEastic) for 
running the demo web service. But this template can be used for everything else 
and does not rely on ILEastic.


### Build
The project gets build with the tool `make`. All necessary objects will be
created with it.

The target library can be passed to the make command as a parameter, _BIN_LIB_.

    make BIN_LIB=MY_LIB

It defaults to the libray `GREETING`. 

The target OS version can be passed with the parameter _TGTRLS_.

Most projects need some common copybooks. The _INCDIR_ parameter refers to the 
IFS folder where the compiler should look for the stream file copybooks. The
default for this is _/usr/local/include_.

The template project expects ILEastic to be installed in the library _ILEASTIC_.
That can be overridden with the parameter _ILEASTIC\_LIB_.

#### Build Single Object
The big advantage of `make` is that you can build groups of objects or just a 
single object. It makes no difference to the tool. Just pass either the build
step or the source name as a parameter.

So for building the Hello World web service go into the folder _src/web_ and
pass `hellows` as a parameter like

    make hellows

It executes the corresponding build command for the registered suffix of the 
file with the basename _hellows_.

### Build Steps
The script supports different steps which can be called separately or together 
in one go.

    make BIN_LIB=MY_LIB purge all

#### Build All
The build step _all_ executes the _env_ make target and then executes the make 
target _all_ in the folders _database_, _services_ and _web_ in that order.

#### Build Environment
In some cases the environment must be prepared like creating and preparing the
various necessary objects, f. e. data areas, data queues, message files, ... .
This is covered with the _env_ make target.

#### Clean Up
To clean up everything after the build you can call the make target _clean_. This
will remove any unnecessary intermediate objects created during the building
of the project, f. e. module objects which are not needed anymore because the
modules are bound by copy into service programs.

#### Purge
If a fresh start is needed for a clean build then the make target _purge_ can be
called. This should remove **every** object created by this build script so that
the project can be build from the start.

Calling the make targets _purge_ and _all_ should always give the same result no
matter how often they are called.


### Start Web Service
You can start the web service with the make target _start_. Make sure that port
44044 is not already occupied, check with `NETSTAT *CNN`. 


### Test Web Service
First you need to enter some test data into the database table _greeting_. Then
you can use your web browser with the following url to test your web service:

    http://<your_server>:44044/api/greeting/<locale>
