#
# Template Project for IBM i
#

#-------------------------------------------------------------------------------
# User-defined part start
#

BIN_LIB=GREETING

#
# User-defined part end
#-------------------------------------------------------------------------------


all: env 
	$(MAKE) -C src/ all $*

clean:
	$(MAKE) -C src/ clean $*

purge:
	$(MAKE) -C src/ purge $*

env:

start:
	system "SBMJOB CMD(CALL PGM($(BIN_LIB)/GREETINGWS)) JOB(GREETING) JOBQ(QUSRNOMAX) CURLIB($(BIN_LIB)) ALWMLTTHD(*YES)"


.PHONY: env clean purge
